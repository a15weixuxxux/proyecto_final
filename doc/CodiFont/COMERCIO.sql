-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost:3306
-- Temps de generació: 24-05-2019 a les 08:40:34
-- Versió del servidor: 5.7.25-0ubuntu0.16.04.2
-- Versió de PHP: 7.2.16-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de dades: `COMERCIO`
--

-- --------------------------------------------------------

--
-- Estructura de la taula `albaran`
--

CREATE TABLE `albaran` (
  `id` int(11) NOT NULL,
  `albalan_numero` int(11) NOT NULL,
  `fecha` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `productos` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cliente_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Bolcant dades de la taula `albaran`
--

INSERT INTO `albaran` (`id`, `albalan_numero`, `fecha`, `productos`, `cliente_id`) VALUES
(3, 1234567891, '23/02/2018', '{.............}', 111),
(4, 985632541, '23/02/2019', '{-----sdsddsd-----}', 44);

-- --------------------------------------------------------

--
-- Estructura de la taula `categoria`
--

CREATE TABLE `categoria` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Bolcant dades de la taula `categoria`
--

INSERT INTO `categoria` (`id`, `name`) VALUES
(3, 'Calcetines'),
(4, 'Calzoncillos'),
(5, 'Niños'),
(6, 'Blanco'),
(7, 'Azul'),
(8, 'Negro'),
(9, 'Marron'),
(11, 'T-shirt'),
(12, 'Boxes'),
(13, 'Hombre'),
(14, 'Mujer'),
(15, 'Pañuelos'),
(17, 'Agua'),
(18, 'Caramelos'),
(19, 'Zumo');

-- --------------------------------------------------------

--
-- Estructura de la taula `cliente`
--

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL,
  `codigo_cliente` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre_cliente` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre_contacto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono_contacto` int(11) NOT NULL,
  `cif_dni_nie` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cp` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ciudad` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provincia` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pais` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono1` int(11) DEFAULT NULL,
  `telefono2` int(11) DEFAULT NULL,
  `movil` int(11) DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direccion_envio` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cp_envio` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ciudad_envio` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provincia_envio` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pais_envio` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descuento_cliente` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Bolcant dades de la taula `cliente`
--

INSERT INTO `cliente` (`id`, `codigo_cliente`, `nombre_cliente`, `nombre_contacto`, `telefono_contacto`, `cif_dni_nie`, `direccion`, `cp`, `ciudad`, `provincia`, `pais`, `telefono1`, `telefono2`, `movil`, `fax`, `email`, `direccion_envio`, `cp_envio`, `ciudad_envio`, `provincia_envio`, `pais_envio`, `descuento_cliente`) VALUES
(1, '00000000', 'Jose', 'Jose', 604454589, '12345678K', 'C/Port Vell 123', '08052', 'Barcelona', 'Barcelona', 'España', 625841326, 625841326, 625841326, '925841326', 'jose@gmail.com', 'C/Port Vell 123', '08052', 'Barcelona', 'Barcelona', 'España', 0),
(4, '123214', 'Weifeng', 'Weifeng', 698523125, 'X1234567X', 'C/Pacific 102 2 2', '08026', 'Barcelona', 'Barcelona', 'España', 693258741, 987456321, 639852125, '987456321', 'a15weixuxxux@iam.cat', 'C/Pacific 102 2 2', '08026', 'Barcelona', 'Barcelona', 'España', 5),
(5, '123456789', 'weiefng', 'Jose', 652384568, 'x324234', 'Carres mediana 123', '08052', 'Barcelona', 'Barcelona', 'España', 652384568, 652384568, 652384568, '652384568', 'asdasd@as.cat', 'Carres mediana 123', '08052', 'Barcelona', 'Barcelona', 'España', 5),
(6, 'ZH123', 'Zehao Chen', 'Zehao', 635241789, 'Z1234567Y', 'C/Industria 175 2 4', '08026', 'Barcelona', 'Barcelona', 'España', 654311785, 698521452, 698540121, '985201410', 'a15zehcheche@iam.cat', 'C/Industria 175 2 4', '08026', 'Barcelona', 'Barcelona', 'España', 3);

-- --------------------------------------------------------

--
-- Estructura de la taula `comercial`
--

CREATE TABLE `comercial` (
  `id` int(11) NOT NULL,
  `codigo_comercio` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre_comercial` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono_comercial` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Bolcant dades de la taula `comercial`
--

INSERT INTO `comercial` (`id`, `codigo_comercio`, `nombre_comercial`, `telefono_comercial`) VALUES
(3, '3333333333', 'Jose', 639582159),
(4, '88669', 'Weifeng', 693258741),
(5, '11256', 'Zehao', 685123657),
(6, '123456', 'Wf', 985123654),
(7, 'WF520', 'Weifeng Xu', 639389100);

-- --------------------------------------------------------

--
-- Estructura de la taula `devolucion`
--

CREATE TABLE `devolucion` (
  `id` int(11) NOT NULL,
  `num_factura` int(11) NOT NULL,
  `fecha` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `productos` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `precio_total` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Bolcant dades de la taula `devolucion`
--

INSERT INTO `devolucion` (`id`, `num_factura`, `fecha`, `productos`, `precio_total`) VALUES
(1, 201900001, '17-5-2019', '[{"codi":1234567891234,"descripcion":"box","Unds":4,"preu":5.5,"descompte":5,"subtotal":"20.90"}]', 23.23),
(2, 201900005, '21-5-2019', '[{"codi":8435516680026,"descripcion":"CALCETÍN TOBILLERO 1TO2001","Unds":"20","preu":0.3,"subtotal":"5.70","descompte":5}]', 5.94),
(3, 201900012, '21-5-2019', '[{"codi":6931925828032,"descripcion":"Kiskis caramelo de menta sin azucar (Melocoton)","quantitatpack":8,"Unds":80,"preu":1.5,"subtotal":"114.00","descompte":5}]', 118.9),
(4, 201900009, '21-5-2019', '[{"codi":8410055050011,"descripcion":"Font Vella Agua Natural 50CL","quantitatpack":6,"Unds":30,"preu":0.5,"subtotal":"14.25","descompte":5}]', 14.86),
(5, 201900015, '23-5-2019', '[{"codi":8435516680019,"descripcion":"CALCETÍN INVISIBLE 1IN2001","quantitatpack":3,"Unds":90,"preu":0.3,"subtotal":"26.19","descompte":3},{"codi":8435516680040,"descripcion":"CALCETIN LARGO 2LA2001","quantitatpack":6,"Unds":180,"preu":0.3,"subtotal":"52.38","descompte":3}]', 81.95);

-- --------------------------------------------------------

--
-- Estructura de la taula `empresa`
--

CREATE TABLE `empresa` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cp` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provincia` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ciudad` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` int(11) NOT NULL,
  `mobil` int(11) NOT NULL,
  `cif` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Bolcant dades de la taula `empresa`
--

INSERT INTO `empresa` (`id`, `nom`, `cp`, `provincia`, `ciudad`, `direccion`, `telefono`, `mobil`, `cif`) VALUES
(1, 'AiMi S.L', '08030', 'Barcelona', 'Barcelona', 'Calle Josep Finestres 12', 984523454, 692125678, 'B-67022147');

-- --------------------------------------------------------

--
-- Estructura de la taula `factura`
--

CREATE TABLE `factura` (
  `id` int(11) NOT NULL,
  `factura_numero` int(11) NOT NULL,
  `fecha` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `productos` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `req` double NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `comercial_id` int(11) NOT NULL,
  `iva` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Bolcant dades de la taula `factura`
--

INSERT INTO `factura` (`id`, `factura_numero`, `fecha`, `productos`, `req`, `cliente_id`, `comercial_id`, `iva`) VALUES
(3, 201900001, '3-5-2019', '[{"codi":1234567891234,"descripcion":"box","Unds":4,"preu":5.5,"descompte":5,"subtotal":"20.90"},{"codi":1234567890123,"descripcion":"Calcetin rayas blancas","Unds":3,"preu":2.65,"descompte":5,"subtotal":"7.55"}]', 5.2, 4, 4, 21),
(6, 201900002, '8-5-2019', '[{"codi":1234567891234,"descripcion":"box","Unds":1,"preu":5.5,"subtotal":"5.50","descompte":0}]', 5.2, 1, 3, 21),
(7, 201900003, '8-5-2019', '[{"codi":1234567891234,"descripcion":"box","Unds":"2","preu":"7.5","subtotal":"14.25","descompte":5}]', 5.2, 4, 4, 21),
(12, 201900004, '10-5-2019', '[{"codi":1234567891235,"descripcion":"MMMMMM","Unds":5,"preu":1,"subtotal":"4.75","descompte":5},{"codi":1234567891234,"descripcion":"box","Unds":4,"preu":5.5,"subtotal":"20.90","descompte":5}]', 5.2, 4, 5, 21),
(34, 201900005, '13-5-2019', '[{"codi":8435516680019,"descripcion":"CALCETÍN INVISIBLE 1IN2001","Unds":"70","preu":0.3,"subtotal":"19.95","descompte":5},{"codi":8435516680026,"descripcion":"CALCETÍN TOBILLERO 1TO2001","Unds":"20","preu":0.3,"subtotal":"5.70","descompte":5}]', 5.2, 4, 4, 21),
(35, 201900006, '13-5-2019', '[{"codi":8435516680019,"descripcion":"CALCETÍN INVISIBLE 1IN2001","Unds":"31","preu":0.3,"subtotal":"8.83","descompte":5}]', 5.2, 4, 4, 21),
(36, 201900007, '13-5-2019', '[{"codi":8435516680019,"descripcion":"CALCETÍN INVISIBLE 1IN2001","quantitatpack":3,"Unds":"5","preu":0.3,"subtotal":"4.28","descompte":5},{"codi":8413585022534,"descripcion":"Pañuelos bonArea 10und.","quantitatpack":12,"Unds":3,"preu":"0.2","subtotal":"6.84","descompte":5}]', 5.2, 4, 4, 21),
(37, 201900008, '13-5-2019', '[{"codi":8435516680019,"descripcion":"CALCETÍN INVISIBLE 1IN2001","quantitatpack":3,"Unds":30,"preu":0.3,"subtotal":"8.55","descompte":5}]', 5.2, 4, 5, 21),
(38, 201900009, '17-5-2019', '[{"codi":8413585022534,"descripcion":"Pañuelos bonArea 10und.","quantitatpack":12,"Unds":60,"preu":0.15,"subtotal":"8.55","descompte":5},{"codi":8410055050011,"descripcion":"Font Vella Agua Natural 50CL","quantitatpack":6,"Unds":30,"preu":0.5,"subtotal":"14.25","descompte":5},{"codi":1234567890123,"descripcion":"Calcetin rayas blancas","quantitatpack":12,"Unds":12,"preu":2.65,"subtotal":"30.21","descompte":5}]', 5.2, 4, 3, 21),
(40, 201900010, '17-4-2019', '[{"codi":8410055050011,"descripcion":"Font Vella Agua Natural 50CL","quantitatpack":6,"Unds":30,"preu":0.5,"subtotal":"15.00","descompte":0}]', 5.2, 1, 4, 21),
(41, 201900011, '21-4-2019', '[{"codi":1234567891234,"descripcion":"box","quantitatpack":12,"Unds":12,"preu":5.5,"subtotal":"62.70","descompte":5}]', 5.2, 4, 3, 21),
(44, 201900012, '21-5-2019', '[{"codi":6931925828032,"descripcion":"Kiskis caramelo de menta sin azucar (Melocoton)","quantitatpack":8,"Unds":80,"preu":1.5,"subtotal":"114.00","descompte":5},{"codi":8410055050011,"descripcion":"Font Vella Agua Natural 50CL","quantitatpack":6,"Unds":30,"preu":0.5,"subtotal":"14.25","descompte":5}]', 5.2, 4, 5, 21),
(45, 201900013, '22-5-2019', '[{"codi":8480000100764,"descripcion":"Hacendado mediterráneo fruta+leche 330ml","quantitatpack":3,"Unds":9,"preu":0.5,"subtotal":"4.37","descompte":3},{"codi":8410055050011,"descripcion":"Font Vella Agua Natural 50CL","quantitatpack":6,"Unds":12,"preu":0.5,"subtotal":"5.82","descompte":3},{"codi":6931925828032,"descripcion":"Kiskis caramelo de menta sin azucar (Melocoton)","quantitatpack":8,"Unds":16,"preu":1.5,"subtotal":"23.28","descompte":3}]', 5.2, 6, 7, 21),
(46, 201900014, '23-5-2019', '[{"codi":8480000100764,"descripcion":"Hacendado mediterráneo fruta+leche 330ml","quantitatpack":3,"Unds":3,"preu":0.5,"subtotal":"1.46","descompte":3}]', 5.2, 6, 7, 21),
(47, 201900015, '23-5-2019', '[{"codi":8480000100764,"descripcion":"Hacendado mediterráneo fruta+leche 330ml","quantitatpack":3,"Unds":9,"preu":0.5,"subtotal":"4.37","descompte":3},{"codi":8410055050011,"descripcion":"Font Vella Agua Natural 50CL","quantitatpack":6,"Unds":30,"preu":0.5,"subtotal":"14.55","descompte":3},{"codi":6931925828032,"descripcion":"Kiskis caramelo de menta sin azucar (Melocoton)","quantitatpack":8,"Unds":16,"preu":1.5,"subtotal":"23.28","descompte":3},{"codi":8435516680019,"descripcion":"CALCETÍN INVISIBLE 1IN2001","quantitatpack":3,"Unds":90,"preu":0.3,"subtotal":"26.19","descompte":3},{"codi":8435516680040,"descripcion":"CALCETIN LARGO 2LA2001","quantitatpack":6,"Unds":180,"preu":0.3,"subtotal":"52.38","descompte":3},{"codi":8435516680071,"descripcion":"CALCETIN LARGO 2LA2004","quantitatpack":6,"Unds":180,"preu":0.3,"subtotal":"52.38","descompte":3}]', 5.2, 6, 7, 21);

-- --------------------------------------------------------

--
-- Estructura de la taula `impuestos`
--

CREATE TABLE `impuestos` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valor` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Bolcant dades de la taula `impuestos`
--

INSERT INTO `impuestos` (`id`, `nom`, `valor`) VALUES
(1, 'iva', 21),
(2, 'req', 5.2);

-- --------------------------------------------------------

--
-- Estructura de la taula `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Bolcant dades de la taula `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20190409122837', '2019-04-09 12:28:49'),
('20190410093805', '2019-04-10 09:38:13'),
('20190410100043', '2019-04-10 10:00:55'),
('20190410102535', '2019-04-12 07:25:56'),
('20190411093331', '2019-04-12 07:27:15'),
('20190423105853', '2019-04-23 11:03:18'),
('20190423114126', '2019-04-23 11:41:35'),
('20190430100512', '2019-04-30 10:05:23');

-- --------------------------------------------------------

--
-- Estructura de la taula `productos`
--

CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `codigo_barra` bigint(20) NOT NULL,
  `codigo_producto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre_producto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `precio_compra` double DEFAULT NULL,
  `pvp` double NOT NULL,
  `cantidad_pack` int(11) DEFAULT NULL,
  `descuento_producto` int(11) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `aviso_stock` int(11) DEFAULT NULL,
  `categoria` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Bolcant dades de la taula `productos`
--

INSERT INTO `productos` (`id`, `codigo_barra`, `codigo_producto`, `nombre_producto`, `precio_compra`, `pvp`, `cantidad_pack`, `descuento_producto`, `stock`, `aviso_stock`, `categoria`) VALUES
(4, 1234567891234, 'MC232110', 'box', 7.5, 5.5, 12, 5, 209, 10, 'Hombre'),
(5, 1234567890123, '2BXL1004', 'Calcetin rayas blancas', 0.75, 2.65, 12, 2, 88, 10, 'Hombre,Calcetin'),
(6, 1234567891235, 'ASC213423', 'MMMMMM', 0.5, 1, 12, 0, 100, 10, 'Calzoncillos,Niños,Hombre'),
(7, 8435516680019, '1IN2001', 'CALCETÍN INVISIBLE 1IN2001', 0.1, 0.3, 3, 0, -36, 10, 'Calcetines,Mujer'),
(8, 8435516680026, '1TO2001', 'CALCETÍN TOBILLERO 1TO2001', 0.1, 0.3, 3, 0, 80, 10, 'Calcetines,Mujer'),
(10, 8435516680033, '2IN2001', 'CALCETÍN INVISIBLE 2IN2001', 0.2, 0.3, 12, 0, 100, 10, 'Calcetines,Hombre'),
(11, 8435516680040, '2LA2001', 'CALCETIN LARGO 2LA2001', 0.2, 0.3, 6, 0, 100, 10, 'Hombre,Calcetines'),
(12, 8435516680057, '2LA2002', 'CALCETIN LARGO 2LA2002', 0.2, 0.3, 6, 0, 100, 10, 'Calcetines,Hombre'),
(13, 8435516680064, '2LA2003', 'CALCETIN LARGO 2LA2003', 0.2, 0.3, 6, 0, 100, 10, 'Hombre,Calcetines'),
(14, 8435516680071, '2LA2004', 'CALCETIN LARGO 2LA2004', 0.2, 0.3, 6, 0, 70, 10, 'Calcetines,Hombre'),
(15, 8435516680088, '2LA2005', 'CALCETIN LARGO 2LA2005', 0.2, 0.3, 6, 0, 100, 10, 'Calcetines,Hombre'),
(16, 8413585022534, '5PA2534', 'Pañuelos bonArea 10und.', 0.1, 0.15, 12, 0, 40, 10, 'Pañuelos'),
(17, 8410055050011, 'FV050011', 'Font Vella Agua Natural 50CL', 0.2, 0.5, 6, 0, 198, 10, 'Agua'),
(18, 6931925828032, 'SB/T 10347', 'Kiskis caramelo de menta sin azucar (Melocoton)', 1.2, 1.5, 8, 0, 496, 10, 'Caramelos'),
(19, 8480000100764, 'HA330ML', 'Hacendado mediterráneo fruta+leche 330ml', 0.3, 0.5, 3, 0, 158, 10, 'Zumo');

--
-- Indexos per taules bolcades
--

--
-- Index de la taula `albaran`
--
ALTER TABLE `albaran`
  ADD PRIMARY KEY (`id`);

--
-- Index de la taula `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id`);

--
-- Index de la taula `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`);

--
-- Index de la taula `comercial`
--
ALTER TABLE `comercial`
  ADD PRIMARY KEY (`id`);

--
-- Index de la taula `devolucion`
--
ALTER TABLE `devolucion`
  ADD PRIMARY KEY (`id`);

--
-- Index de la taula `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`id`);

--
-- Index de la taula `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`id`);

--
-- Index de la taula `impuestos`
--
ALTER TABLE `impuestos`
  ADD PRIMARY KEY (`id`);

--
-- Index de la taula `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index de la taula `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT per les taules bolcades
--

--
-- AUTO_INCREMENT per la taula `albaran`
--
ALTER TABLE `albaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT per la taula `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT per la taula `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT per la taula `comercial`
--
ALTER TABLE `comercial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT per la taula `devolucion`
--
ALTER TABLE `devolucion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT per la taula `empresa`
--
ALTER TABLE `empresa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la taula `factura`
--
ALTER TABLE `factura`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT per la taula `impuestos`
--
ALTER TABLE `impuestos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT per la taula `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
