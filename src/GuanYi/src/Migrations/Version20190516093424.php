<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190516093424 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE devolucion (id INT AUTO_INCREMENT NOT NULL, num_factura INT NOT NULL, fecha VARCHAR(255) NOT NULL, productos VARCHAR(255) NOT NULL, precio_total DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE factura CHANGE productos productos VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE empresa CHANGE cp cp VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE cliente CHANGE cp cp VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE devolucion');
        $this->addSql('ALTER TABLE cliente CHANGE cp cp VARCHAR(11) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE empresa CHANGE cp cp VARCHAR(8) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE factura CHANGE productos productos LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
