<?php

namespace App\Repository;

use App\Entity\EMPRESA;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EMPRESA|null find($id, $lockMode = null, $lockVersion = null)
 * @method EMPRESA|null findOneBy(array $criteria, array $orderBy = null)
 * @method EMPRESA[]    findAll()
 * @method EMPRESA[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EMPRESARepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EMPRESA::class);
    }

    // /**
    //  * @return EMPRESA[] Returns an array of EMPRESA objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EMPRESA
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
