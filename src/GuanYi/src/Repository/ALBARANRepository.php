<?php

namespace App\Repository;

use App\Entity\ALBARAN;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ALBARAN|null find($id, $lockMode = null, $lockVersion = null)
 * @method ALBARAN|null findOneBy(array $criteria, array $orderBy = null)
 * @method ALBARAN[]    findAll()
 * @method ALBARAN[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ALBARANRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ALBARAN::class);
    }

    // /**
    //  * @return ALBARAN[] Returns an array of ALBARAN objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ALBARAN
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
