<?php

namespace App\Repository;

use App\Entity\CATEGORIA;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CATEGORIA|null find($id, $lockMode = null, $lockVersion = null)
 * @method CATEGORIA|null findOneBy(array $criteria, array $orderBy = null)
 * @method CATEGORIA[]    findAll()
 * @method CATEGORIA[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CATEGORIARepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CATEGORIA::class);
    }

    // /**
    //  * @return CATEGORIA[] Returns an array of CATEGORIA objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CATEGORIA
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
