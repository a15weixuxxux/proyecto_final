<?php

namespace App\Repository;

use App\Entity\DEVOLUCION;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DEVOLUCION|null find($id, $lockMode = null, $lockVersion = null)
 * @method DEVOLUCION|null findOneBy(array $criteria, array $orderBy = null)
 * @method DEVOLUCION[]    findAll()
 * @method DEVOLUCION[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DEVOLUCIONRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DEVOLUCION::class);
    }

    // /**
    //  * @return DEVOLUCION[] Returns an array of DEVOLUCION objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DEVOLUCION
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
