<?php

namespace App\Repository;

use App\Entity\CLIENTE;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CLIENTE|null find($id, $lockMode = null, $lockVersion = null)
 * @method CLIENTE|null findOneBy(array $criteria, array $orderBy = null)
 * @method CLIENTE[]    findAll()
 * @method CLIENTE[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CLIENTERepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CLIENTE::class);
    }

    // /**
    //  * @return CLIENTE[] Returns an array of CLIENTE objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CLIENTE
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
