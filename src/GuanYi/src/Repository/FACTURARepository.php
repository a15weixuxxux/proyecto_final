<?php

namespace App\Repository;

use App\Entity\FACTURA;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method FACTURA|null find($id, $lockMode = null, $lockVersion = null)
 * @method FACTURA|null findOneBy(array $criteria, array $orderBy = null)
 * @method FACTURA[]    findAll()
 * @method FACTURA[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FACTURARepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, FACTURA::class);
    }

    // /**
    //  * @return FACTURA[] Returns an array of FACTURA objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FACTURA
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
