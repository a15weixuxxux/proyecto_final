<?php

namespace App\Repository;

use App\Entity\COMERCIAL;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method COMERCIAL|null find($id, $lockMode = null, $lockVersion = null)
 * @method COMERCIAL|null findOneBy(array $criteria, array $orderBy = null)
 * @method COMERCIAL[]    findAll()
 * @method COMERCIAL[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class COMERCIALRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, COMERCIAL::class);
    }

    // /**
    //  * @return COMERCIAL[] Returns an array of COMERCIAL objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?COMERCIAL
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
