<?php

namespace App\Repository;

use App\Entity\PRODUCTOS;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PRODUCTOS|null find($id, $lockMode = null, $lockVersion = null)
 * @method PRODUCTOS|null findOneBy(array $criteria, array $orderBy = null)
 * @method PRODUCTOS[]    findAll()
 * @method PRODUCTOS[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PRODUCTOSRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PRODUCTOS::class);
    }

    // /**
    //  * @return PRODUCTOS[] Returns an array of PRODUCTOS objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PRODUCTOS
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
