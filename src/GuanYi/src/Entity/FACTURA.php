<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FACTURARepository")
 */
class FACTURA
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $facturaNumero;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fecha;

    /**
     * @ORM\Column(type="string")
     */
    private $productos;

    /**
     * @ORM\Column(type="float")
     */
    private $req;

    /**
     * @ORM\Column(type="integer")
     */
    private $clienteId;

    /**
     * @ORM\Column(type="integer")
     */
    private $comercialId;


    /**
     * @ORM\Column(type="float")
     */
    private $iva;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFacturaNumero(): ?int
    {
        return $this->facturaNumero;
    }

    public function setFacturaNumero(int $facturaNumero): self
    {
        $this->facturaNumero = $facturaNumero;

        return $this;
    }

    public function getFecha(): ?string
    {
        return $this->fecha;
    }

    public function setFecha(string $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getProductos(): ?string
    {
        return $this->productos;
    }

    public function setProductos(string $productos): self
    {
        $this->productos = $productos;

        return $this;
    }


    public function getReq(): ?float
    {
        return $this->req;
    }

    public function setReq(float $req): self
    {
        $this->req = $req;

        return $this;
    }

    public function getClienteId(): ?int
    {
        return $this->clienteId;
    }

    public function setClienteId(int $clienteId): self
    {
        $this->clienteId = $clienteId;

        return $this;
    }

    public function getComercialId(): ?int
    {
        return $this->comercialId;
    }

    public function setComercialId(int $comercialId): self
    {
        $this->comercialId = $comercialId;

        return $this;
    }

    public function getIva(): ?float
    {
        return $this->iva;
    }

    public function setIva(float $iva): self
    {
        $this->iva = $iva;

        return $this;
    }
}
