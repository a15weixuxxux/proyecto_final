<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PRODUCTOSRepository")
 */
class PRODUCTOS
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $CODIGO_PRODUCTO;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $NOMBRE_PRODUCTO;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $PRECIO_COMPRA;

    /**
     * @ORM\Column(type="float")
     */
    private $PVP;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $CANTIDAD_PACK;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $DESCUENTO_PRODUCTO;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $STOCK;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $AVISO_STOCK;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $CATEGORIA;

    /**
     * @ORM\Column(type="bigint")
     */
    private $CODIGO_BARRA;
    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCODIGOPRODUCTO(): ?string
    {
        return $this->CODIGO_PRODUCTO;
    }

    public function setCODIGOPRODUCTO(string $CODIGO_PRODUCTO): self
    {
        $this->CODIGO_PRODUCTO = $CODIGO_PRODUCTO;

        return $this;
    }

    public function getNOMBREPRODUCTO(): ?string
    {
        return $this->NOMBRE_PRODUCTO;
    }

    public function setNOMBREPRODUCTO(string $NOMBRE_PRODUCTO): self
    {
        $this->NOMBRE_PRODUCTO = $NOMBRE_PRODUCTO;

        return $this;
    }

    public function getPRECIOCOMPRA(): ?float
    {
        return $this->PRECIO_COMPRA;
    }

    public function setPRECIOCOMPRA(?float $PRECIO_COMPRA): self
    {
        $this->PRECIO_COMPRA = $PRECIO_COMPRA;

        return $this;
    }

    public function getPVP(): ?float
    {
        return $this->PVP;
    }

    public function setPVP(float $PVP): self
    {
        $this->PVP = $PVP;

        return $this;
    }

    public function getCANTIDADPACK(): ?int
    {
        return $this->CANTIDAD_PACK;
    }

    public function setCANTIDADPACK(?int $CANTIDAD_PACK): self
    {
        $this->CANTIDAD_PACK = $CANTIDAD_PACK;

        return $this;
    }

    public function getDESCUENTOPRODUCTO(): ?int
    {
        return $this->DESCUENTO_PRODUCTO;
    }

    public function setDESCUENTOPRODUCTO(?int $DESCUENTO_PRODUCTO): self
    {
        $this->DESCUENTO_PRODUCTO = $DESCUENTO_PRODUCTO;

        return $this;
    }

    public function getSTOCK(): ?int
    {
        return $this->STOCK;
    }

    public function setSTOCK(?int $STOCK): self
    {
        $this->STOCK = $STOCK;

        return $this;
    }

    public function getAVISOSTOCK(): ?int
    {
        return $this->AVISO_STOCK;
    }

    public function setAVISOSTOCK(?int $AVISO_STOCK): self
    {
        $this->AVISO_STOCK = $AVISO_STOCK;

        return $this;
    }

    public function getCATEGORIA(): ?string
    {
        return $this->CATEGORIA;
    }

    public function setCATEGORIA(?string $CATEGORIA): self
    {
        $this->CATEGORIA = $CATEGORIA;

        return $this;
    }

    public function getCODIGOBARRA(): ?int
    {
        return $this->CODIGO_BARRA;
    }

    public function setCODIGOBARRA(int $CODIGO_BARRA): self
    {
        $this->CODIGO_BARRA = $CODIGO_BARRA;

        return $this;
    }

}
