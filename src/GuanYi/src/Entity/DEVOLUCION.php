<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DEVOLUCIONRepository")
 */
class DEVOLUCION
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $numFactura;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fecha;

    /**
     * @ORM\Column(type="string")
     */
    private $productos;

    /**
     * @ORM\Column(type="float")
     */
    private $precioTotal;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumFactura(): ?int
    {
        return $this->numFactura;
    }

    public function setNumFactura(int $numFactura): self
    {
        $this->numFactura = $numFactura;

        return $this;
    }

    public function getFecha(): ?string
    {
        return $this->fecha;
    }

    public function setFecha(string $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getProductos(): ?string
    {
        return $this->productos;
    }

    public function setProductos(string $productos): self
    {
        $this->productos = $productos;

        return $this;
    }

    public function getPrecioTotal(): ?float
    {
        return $this->precioTotal;
    }

    public function setPrecioTotal(float $precioTotal): self
    {
        $this->precioTotal = $precioTotal;

        return $this;
    }
}
