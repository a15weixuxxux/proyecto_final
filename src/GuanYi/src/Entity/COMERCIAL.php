<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\COMERCIALRepository")
 */
class COMERCIAL
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $CODIGO_COMERCIO;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $NOMBRE_COMERCIAL;

    /**
     * @ORM\Column(type="integer")
     */
    private $TELEFONO_COMERCIAL;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCODIGOCOMERCIO(): ?string
    {
        return $this->CODIGO_COMERCIO;
    }

    public function setCODIGOCOMERCIO(string $CODIGO_COMERCIO): self
    {
        $this->CODIGO_COMERCIO = $CODIGO_COMERCIO;

        return $this;
    }

    public function getNOMBRECOMERCIAL(): ?string
    {
        return $this->NOMBRE_COMERCIAL;
    }

    public function setNOMBRECOMERCIAL(string $NOMBRE_COMERCIAL): self
    {
        $this->NOMBRE_COMERCIAL = $NOMBRE_COMERCIAL;

        return $this;
    }

    public function getTELEFONOCOMERCIAL(): ?int
    {
        return $this->TELEFONO_COMERCIAL;
    }

    public function setTELEFONOCOMERCIAL(int $TELEFONO_COMERCIAL): self
    {
        $this->TELEFONO_COMERCIAL = $TELEFONO_COMERCIAL;

        return $this;
    }
}
