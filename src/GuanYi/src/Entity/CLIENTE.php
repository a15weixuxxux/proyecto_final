<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CLIENTERepository")
 */
class CLIENTE
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $CODIGO_CLIENTE;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $NOMBRE_CLIENTE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $NOMBRE_CONTACTO;

    /**
     * @ORM\Column(type="integer")
     */
    private $TELEFONO_CONTACTO;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $CIF_DNI_NIE;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $DIRECCION;

    /**
     * @ORM\Column(type="string")
     */
    private $CP;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $CIUDAD;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $PROVINCIA;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $PAIS;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $TELEFONO1;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $TELEFONO2;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $MOVIL;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $FAX;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $EMAIL;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $DIRECCION_ENVIO;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $CP_ENVIO;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $CIUDAD_ENVIO;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $PROVINCIA_ENVIO;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $PAIS_ENVIO;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $DESCUENTO_CLIENTE;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCODIGOCLIENTE(): ?string
    {
        return $this->CODIGO_CLIENTE;
    }

    public function setCODIGOCLIENTE(string $CODIGO_CLIENTE): self
    {
        $this->CODIGO_CLIENTE = $CODIGO_CLIENTE;

        return $this;
    }

    public function getNOMBRECLIENTE(): ?string
    {
        return $this->NOMBRE_CLIENTE;
    }

    public function setNOMBRECLIENTE(string $NOMBRE_CLIENTE): self
    {
        $this->NOMBRE_CLIENTE = $NOMBRE_CLIENTE;

        return $this;
    }

    public function getNOMBRECONTACTO(): ?string
    {
        return $this->NOMBRE_CONTACTO;
    }

    public function setNOMBRECONTACTO(?string $NOMBRE_CONTACTO): self
    {
        $this->NOMBRE_CONTACTO = $NOMBRE_CONTACTO;

        return $this;
    }

    public function getTELEFONOCONTACTO(): ?int
    {
        return $this->TELEFONO_CONTACTO;
    }

    public function setTELEFONOCONTACTO(int $TELEFONO_CONTACTO): self
    {
        $this->TELEFONO_CONTACTO = $TELEFONO_CONTACTO;

        return $this;
    }

    public function getCIFDNINIE(): ?string
    {
        return $this->CIF_DNI_NIE;
    }

    public function setCIFDNINIE(string $CIF_DNI_NIE): self
    {
        $this->CIF_DNI_NIE = $CIF_DNI_NIE;

        return $this;
    }

    public function getDIRECCION(): ?string
    {
        return $this->DIRECCION;
    }

    public function setDIRECCION(string $DIRECCION): self
    {
        $this->DIRECCION = $DIRECCION;

        return $this;
    }

    public function getCP(): ?string
    {
        return $this->CP;
    }

    public function setCP(string $CP): self
    {
        $this->CP = $CP;

        return $this;
    }

    public function getCIUDAD(): ?string
    {
        return $this->CIUDAD;
    }

    public function setCIUDAD(string $CIUDAD): self
    {
        $this->CIUDAD = $CIUDAD;

        return $this;
    }

    public function getPROVINCIA(): ?string
    {
        return $this->PROVINCIA;
    }

    public function setPROVINCIA(string $PROVINCIA): self
    {
        $this->PROVINCIA = $PROVINCIA;

        return $this;
    }

    public function getPAIS(): ?string
    {
        return $this->PAIS;
    }

    public function setPAIS(string $PAIS): self
    {
        $this->PAIS = $PAIS;

        return $this;
    }

    public function getTELEFONO1(): ?int
    {
        return $this->TELEFONO1;
    }

    public function setTELEFONO1(?int $TELEFONO1): self
    {
        $this->TELEFONO1 = $TELEFONO1;

        return $this;
    }

    public function getTELEFONO2(): ?int
    {
        return $this->TELEFONO2;
    }

    public function setTELEFONO2(?int $TELEFONO2): self
    {
        $this->TELEFONO2 = $TELEFONO2;

        return $this;
    }

    public function getMOVIL(): ?int
    {
        return $this->MOVIL;
    }

    public function setMOVIL(?int $MOVIL): self
    {
        $this->MOVIL = $MOVIL;

        return $this;
    }

    public function getFAX(): ?string
    {
        return $this->FAX;
    }

    public function setFAX(?string $FAX): self
    {
        $this->FAX = $FAX;

        return $this;
    }

    public function getEMAIL(): ?string
    {
        return $this->EMAIL;
    }

    public function setEMAIL(?string $EMAIL): self
    {
        $this->EMAIL = $EMAIL;

        return $this;
    }

    public function getDIRECCIONENVIO(): ?string
    {
        return $this->DIRECCION_ENVIO;
    }

    public function setDIRECCIONENVIO(string $DIRECCION_ENVIO): self
    {
        $this->DIRECCION_ENVIO = $DIRECCION_ENVIO;

        return $this;
    }

    public function getCPENVIO(): ?string
    {
        return $this->CP_ENVIO;
    }

    public function setCPENVIO(string $CP_ENVIO): self
    {
        $this->CP_ENVIO = $CP_ENVIO;

        return $this;
    }

    public function getCIUDADENVIO(): ?string
    {
        return $this->CIUDAD_ENVIO;
    }

    public function setCIUDADENVIO(string $CIUDAD_ENVIO): self
    {
        $this->CIUDAD_ENVIO = $CIUDAD_ENVIO;

        return $this;
    }

    public function getPROVINCIAENVIO(): ?string
    {
        return $this->PROVINCIA_ENVIO;
    }

    public function setPROVINCIAENVIO(string $PROVINCIA_ENVIO): self
    {
        $this->PROVINCIA_ENVIO = $PROVINCIA_ENVIO;

        return $this;
    }

    public function getPAISENVIO(): ?string
    {
        return $this->PAIS_ENVIO;
    }

    public function setPAISENVIO(string $PAIS_ENVIO): self
    {
        $this->PAIS_ENVIO = $PAIS_ENVIO;

        return $this;
    }

    public function getDESCUENTOCLIENTE(): ?int
    {
        return $this->DESCUENTO_CLIENTE;
    }

    public function setDESCUENTOCLIENTE(?int $DESCUENTO_CLIENTE): self
    {
        $this->DESCUENTO_CLIENTE = $DESCUENTO_CLIENTE;

        return $this;
    }
}
