<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ALBARANRepository")
 */
class ALBARAN
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $albalanNumero;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fecha;

    /**
     * @ORM\Column(type="string")
     */
    private $productos;

    /**
     * @ORM\Column(type="integer")
     */
    private $clienteId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAlbalanNumero(): ?int
    {
        return $this->albalanNumero;
    }

    public function setAlbalanNumero(int $albalanNumero): self
    {
        $this->albalanNumero = $albalanNumero;

        return $this;
    }

    public function getFecha(): ?string
    {
        return $this->fecha;
    }

    public function setFecha(string $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getProductos(): ?string
    {
        return $this->productos;
    }

    public function setProductos(string $productos): self
    {
        $this->productos = $productos;

        return $this;
    }

    public function getClienteId(): ?int
    {
        return $this->clienteId;
    }

    public function setClienteId(int $clienteId): self
    {
        $this->clienteId = $clienteId;

        return $this;
    }
}
