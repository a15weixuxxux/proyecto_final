<?php
/**
 * Created by PhpStorm.
 * User: ausias
 * Date: 12/04/19
 * Time: 08:39
 */
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\PRODUCTOS;
class ProductoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('_codigo__barra')
            ->add('_codigo__producto')
            ->add('_nombre__producto')
            ->add('_precio__compra')
            ->add('_pvp')
            ->add('_cantidad__pack')
            ->add('_descuento__producto')
            ->add('_stock')
            ->add('_aviso__stock')
            ->add('_categoria')
            ->add('save', SubmitType::class)
        ;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => PRODUCTOS::class,
            'csrf_protection' => false
        ));
    }
}