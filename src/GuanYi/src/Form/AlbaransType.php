<?php
/**
 * Created by PhpStorm.
 * User: ausias
 * Date: 12/04/19
 * Time: 08:39
 */
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\ALBARAN;
class AlbaransType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('albalan_numero')
            ->add('fecha')
            ->add('productos')
            ->add('cliente_id')
            ->add('save', SubmitType::class)
        ;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ALBARAN::class,
            'csrf_protection' => false
        ));
    }
}