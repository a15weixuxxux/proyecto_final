<?php
/**
 * Created by PhpStorm.
 * User: ausias
 * Date: 12/04/19
 * Time: 08:39
 */
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\CLIENTE;
class ClienteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('_codigo__cliente')
            ->add('_nombre__cliente')
            ->add('_nombre__contacto')
            ->add('_telefono__contacto')
            ->add('_cif__dni__nie')
            ->add('_direccion')
            ->add('_cp')
            ->add('_ciudad')
            ->add('_provincia')
            ->add('_pais')
            ->add('_telefono1')
            ->add('_telefono2')
            ->add('_movil')
            ->add('_fax')
            ->add('_email')
            ->add('_direccion__envio')
            ->add('_cp__envio')
            ->add('_ciudad__envio')
            ->add('_provincia__envio')
            ->add('_pais__envio')
            ->add('_descuento__cliente')
            ->add('save', SubmitType::class)
        ;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => CLIENTE::class,
            'csrf_protection' => false
        ));
    }
}