<?php
/**
 * Created by PhpStorm.
 * User: ausias
 * Date: 12/04/19
 * Time: 08:39
 */
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\EMPRESA;
class EmpresaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('nom')
            ->add('cp')
            ->add('provincia')
            ->add('ciudad')
            ->add('direccion')
            ->add('telefono')
            ->add('mobil')
            ->add('cif')
            ->add('save', SubmitType::class)
        ;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => EMPRESA::class,
            'csrf_protection' => false
        ));
    }
}