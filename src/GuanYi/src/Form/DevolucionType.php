<?php
/**
 * Created by PhpStorm.
 * User: ausias
 * Date: 16/05/19
 * Time: 11:10
 */

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Devolucion;
class DevolucionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('numFactura')
            ->add('fecha')
            ->add('productos')
            ->add('precioTotal')
            ->add('save', SubmitType::class)
        ;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Devolucion::class,
            'csrf_protection' => false
        ));
    }
}