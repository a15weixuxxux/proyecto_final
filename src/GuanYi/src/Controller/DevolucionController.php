<?php
/**
 * Created by PhpStorm.
 * User: ausias
 * Date: 16/05/19
 * Time: 11:03
 */


namespace App\Controller;

use App\Form\DevolucionType;
use App\Form\FacturaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\DEVOLUCION;
use App\Entity\PRODUCTOS;
use App\Entity\FACTURA;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * @Route("/api", name="api_")
 */
class DevolucionController extends FOSRestController
{
    /**
     * @Rest\Get("/devolucions")
     *
     * @return Response
     */
    public function getAction()
    {
        $repository = $this->getDoctrine()->getRepository(DEVOLUCION::class);
        $repositoryF = $this->getDoctrine()->getRepository(FACTURA::class);

//      $devolucion = $repository->findall();
        $devolucion = $repository->createQueryBuilder('devolucion')->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);


        for($i=0;$i<count($devolucion);$i++){
            $num_factura = $devolucion[$i]["numFactura"];
            $devolucion[$i] = (array) $devolucion[$i];
            $devolucion[$i]["fecha_factura"] =
                $repositoryF->createQueryBuilder('factura')
                ->where('factura.facturaNumero = :num_factura')
                ->setParameter('num_factura', $num_factura)
                ->getQuery()
                ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY)[0]["fecha"];

        }
        return new JsonResponse($devolucion);
        //return $this->handleView($this->view($devolucion));
    }
    /**
     * @Rest\Get("/devolucion/{numFactura}")
     *
     * @return Response
     */
    public function getByIdAction($numFactura)
    {
        $repository = $this->getDoctrine()->getRepository(DEVOLUCION::class);
        $factura =  $repository->findBy(['numFactura'=>$numFactura]);
        if (!$factura) {
            return $this->handleView($this->view(['Error' => 'factura not found'], Response::HTTP_CREATED));

            //throw $this->createNotFoundException(sprintf('No Movie found with id "%s"', $id ));
        }
        return $this->handleView($this->view($factura));
    }


    /**
     * @Rest\Post("/devolucion")
     *
     * @return Response
     */
    public function postAction(Request $request)
    {
        $repositoryP = $this->getDoctrine()->getRepository(PRODUCTOS::class);

        $devolucion = new DEVOLUCION();
        $form = $this->createForm(DevolucionType::class, $devolucion);
        $data = json_decode($request->getContent(), true);
        $form->submit($data);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            // gestio de estoc de producte
            $productos =  (Array) json_decode($devolucion->getProductos());
            foreach ($productos as $producto) {
                $productoBD =  $repositoryP->findOneBy(["CODIGO_BARRA" => $producto->codi]);
                $productoBD->setSTOCK($productoBD->getSTOCK() + ($producto->Unds / $producto->quantitatpack));
                $em->persist($productoBD);
                $em->flush();
            }

            $em->persist($devolucion);
            $em->flush();
            return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
        }
        return $this->handleView($this->view($form->getErrors()));
    }
    /**
     * @Rest\Put("/devolucion/{id}")
     *
     * @return Response
     */
    public function updateAction(Request $request,$id)
    {
        $repository = $this->getDoctrine()->getRepository(DEVOLUCION::class);
        $factura =  $repository->find($id);

        if (!$factura) {
            return $this->handleView($this->view(['Error' => 'Categoria not found'], Response::HTTP_CREATED));
            /*
            throw $this->createNotFoundException(sprintf(
                'No Movie found with id "%s"',
                $id
            ));*/
        }

        $factura_numero = $request->get('factura_numero');
        $fecha = $request->get('fecha');
        $productos = $request->get('productos');
        $req = $request->get('req');
        $cliente_id = $request->get('cliente_id');
        $comercial_id = $request->get('comercial_id');
        $iva = $request->get('iva');


        $em = $this->getDoctrine()->getManager();
        if(!empty($factura_numero)){
            $factura->setFacturaNumero($factura_numero);
        }
        if(!empty($fecha)){
            $factura->setFecha($fecha);
        }
        if(!empty($productos)){
            $factura->setProductos($productos);
        }
        if(!empty($req)){
            $factura->setReq($req);
        }
        if(!empty($cliente_id)){
            $factura->setClienteId($cliente_id);
        }
        if(!empty($comercial_id)){
            $factura->getComercialId($comercial_id);
        }
        if(!empty($iva)){
            $factura->setIva($iva);
        }
        $em->flush();

        return $this->handleView($this->view(['Updated' => 'Successfully'], Response::HTTP_CREATED));

    }

    /**
     * @Rest\Delete("/devolucion/{id}")
     *
     * @return Response
     */
    public function deleteComercialAction($id)
    {
        $repository = $this->getDoctrine()->getRepository(DEVOLUCION::class);
        $factura =  $repository->find($id);
        $em = $this->getDoctrine()->getManager();

        if (empty($factura)) {
            return $this->handleView($this->view(['Error' => 'Categoria not found'], Response::HTTP_CREATED));
        }
        else {
            $em->remove($factura);
            $em->flush();
        }
        return $this->handleView($this->view(['Deleted' => 'Successfully'], Response::HTTP_CREATED));

    }

}
