<?php
/**
 * Created by PhpStorm.
 * User: ausias
 * Date: 10/04/19
 * Time: 11:39
 */

namespace App\Controller;

use App\Form\FacturaType;
use App\Form\ImpuestosType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Impuestos;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/api", name="api_")
 */
class ImpuestosController extends FOSRestController
{
    /**
     * @Rest\Get("/impuestos")
     *
     * @return Response
     */
    public function getAction()
    {
        $repository = $this->getDoctrine()->getRepository(Impuestos::class);
        $factura = $repository->findall();
        return $this->handleView($this->view($factura));
    }
    /**
     * @Rest\Get("/impuesto/{id}")
     *
     * @return Response
     */
    public function getByIdAction($id)
    {
        $repository = $this->getDoctrine()->getRepository(Impuestos::class);
        $factura =  $repository->find($id);
        if (!$factura) {
            return $this->handleView($this->view(['Error' => 'factura not found'], Response::HTTP_CREATED));

            //throw $this->createNotFoundException(sprintf('No Movie found with id "%s"', $id ));
        }
        return $this->handleView($this->view($factura));
    }


    /**
     * @Rest\Post("/impuesto")
     *
     * @return Response
     */
    public function postAction(Request $request)
    {
        $impuesto = new Impuestos();
        $form = $this->createForm(ImpuestosType::class, $impuesto);
        $data = json_decode($request->getContent(), true);
        $form->submit($data);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($impuesto);
            $em->flush();
            return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
        }
        return $this->handleView($this->view($form->getErrors()));
    }
    /**
     * @Rest\Put("/impuesto/{id}")
     *
     * @return Response
     */
    public function updateAction(Request $request,$id)
    {
        $repository = $this->getDoctrine()->getRepository(Impuestos::class);
        $impuesto =  $repository->find($id);

        if (!$impuesto) {
            return $this->handleView($this->view(['Error' => 'Categoria not found'], Response::HTTP_CREATED));
            /*
            throw $this->createNotFoundException(sprintf(
                'No Movie found with id "%s"',
                $id
            ));*/
        }
        
        $nom = $request->get('nom');
        $valor = $request->get('valor');


        $em = $this->getDoctrine()->getManager();
        if(!empty($nom)){
            $impuesto->setNom($nom);
        }
        if(!empty($valor)){
            $impuesto->setValor($valor);
        }

        $em->flush();

        return $this->handleView($this->view(['Updated' => 'Successfully'], Response::HTTP_CREATED));

    }

    /**
     * @Rest\Delete("/impuesto/{id}")
     *
     * @return Response
     */
    public function deleteComercialAction($id)
    {
        $repository = $this->getDoctrine()->getRepository(Impuestos::class);
        $impuesto =  $repository->find($id);
        $em = $this->getDoctrine()->getManager();

        if (empty($impuesto)) {
            return $this->handleView($this->view(['Error' => 'Categoria not found'], Response::HTTP_CREATED));
        }
        else {
            $em->remove($impuesto);
            $em->flush();
        }
        return $this->handleView($this->view(['Deleted' => 'Successfully'], Response::HTTP_CREATED));

    }

}
