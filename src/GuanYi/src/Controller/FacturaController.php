<?php
/**
 * Created by PhpStorm.
 * User: ausias
 * Date: 10/04/19
 * Time: 11:39
 */

namespace App\Controller;

use App\Form\FacturaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\FACTURA;
use App\Entity\PRODUCTOS;
use App\Entity\CLIENTE;
use App\Entity\COMERCIAL;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Doctrine\ORM\Query;
use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * @Route("/api", name="api_")
 */
class FacturaController extends FOSRestController
{
    /**
     * @Rest\Get("/facturas")
     *
     * @return Response
     */
    public function getAction()
    {
        $repository = $this->getDoctrine()->getRepository(FACTURA::class);
        $repositoryClient = $this->getDoctrine()->getRepository(CLIENTE::class);
        $repositoryComercial = $this->getDoctrine()->getRepository(COMERCIAL::class);

        $facturas = $repository->createQueryBuilder('factura')->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        for($i=0;$i<count($facturas);$i++){
            $clinentID = $facturas[$i]["clienteId"];
            $comercialID = $facturas[$i]["comercialId"];
            $facturas[$i] = (array) $facturas[$i];
            $facturas[$i]["client"] = $repositoryClient->createQueryBuilder('client')
                ->where('client.id = :id')
                ->setParameter('id', $clinentID)
                ->getQuery()
                ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY)[0];
            $facturas[$i]["comercial"] = $repositoryComercial->createQueryBuilder('comercial')
                ->where('comercial.id = :id')
                ->setParameter('id', $comercialID)
                ->getQuery()
                ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY)[0];

        }
        return new JsonResponse($facturas);






        return $this->handleView($this->view($facturas));

    }
    /**
     * @Rest\Get("/factura/{numFactura}")
     *
     * @return Response
     */
    public function getByIdAction($numFactura)
    {
        $repository = $this->getDoctrine()->getRepository(FACTURA::class);
        $factura =  $repository->findBy(['facturaNumero'=>$numFactura]);
        if (!$factura) {
            return $this->handleView($this->view(['Error' => 'factura not found'], Response::HTTP_CREATED));

            //throw $this->createNotFoundException(sprintf('No Movie found with id "%s"', $id ));
        }
        return $this->handleView($this->view($factura[0]));
    }


    /**
     * @Rest\Post("/factura")
     *
     * @return Response
     */
    public function postAction(Request $request)
    {
        $repositoryP = $this->getDoctrine()->getRepository(PRODUCTOS::class);

        $factura = new FACTURA();
        $form = $this->createForm(FacturaType::class, $factura);
        $data = json_decode($request->getContent(), true);
        $form->submit($data);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            // gestio de estoc de producte
            $productos =  (Array) json_decode($factura->getProductos());
            foreach ($productos as $producto) {
                $productoBD =  $repositoryP->findOneBy(["CODIGO_BARRA" => $producto->codi]);
                $productoBD->setSTOCK($productoBD->getSTOCK() - ($producto->Unds / $producto->quantitatpack) );
                $em->persist($productoBD);
                $em->flush();
            }

            $em->persist($factura);
            $em->flush();
            return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
        }
        return $this->handleView($this->view($form->getErrors()));
    }
    /**
     * @Rest\Put("/factura/{id}")
     *
     * @return Response
     */
    public function updateAction(Request $request,$id)
    {
        $repository = $this->getDoctrine()->getRepository(FACTURA::class);
        $factura =  $repository->find($id);

        if (!$factura) {
            return $this->handleView($this->view(['Error' => 'Categoria not found'], Response::HTTP_CREATED));
            /*
            throw $this->createNotFoundException(sprintf(
                'No Movie found with id "%s"',
                $id
            ));*/
        }

        $factura_numero = $request->get('factura_numero');
        $fecha = $request->get('fecha');
        $productos = $request->get('productos');
        $req = $request->get('req');
        $cliente_id = $request->get('cliente_id');
        $comercial_id = $request->get('comercial_id');
        $iva = $request->get('iva');


        $em = $this->getDoctrine()->getManager();
        if(!empty($factura_numero)){
            $factura->setFacturaNumero($factura_numero);
        }
        if(!empty($fecha)){
            $factura->setFecha($fecha);
        }
        if(!empty($productos)){
            $factura->setProductos($productos);
        }
        if(!empty($req)){
            $factura->setReq($req);
        }
        if(!empty($cliente_id)){
            $factura->setClienteId($cliente_id);
        }
        if(!empty($comercial_id)){
            $factura->getComercialId($comercial_id);
        }
        if(!empty($iva)){
            $factura->setIva($iva);
        }
        $em->flush();

        return $this->handleView($this->view(['Updated' => 'Successfully'], Response::HTTP_CREATED));

    }

    /**
     * @Rest\Delete("/factura/{id}")
     *
     * @return Response
     */
    public function deleteComercialAction($id)
    {
        $repository = $this->getDoctrine()->getRepository(FACTURA::class);
        $factura =  $repository->find($id);
        $em = $this->getDoctrine()->getManager();

        if (empty($factura)) {
            return $this->handleView($this->view(['Error' => 'Categoria not found'], Response::HTTP_CREATED));
        }
        else {
            $em->remove($factura);
            $em->flush();
        }
        return $this->handleView($this->view(['Deleted' => 'Successfully'], Response::HTTP_CREATED));

    }

}
