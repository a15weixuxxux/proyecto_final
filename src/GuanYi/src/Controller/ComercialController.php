<?php
/**
 * Created by PhpStorm.
 * User: ausias
 * Date: 10/04/19
 * Time: 11:39
 */

namespace App\Controller;
use App\Form\CategoriaType;
use App\Form\ComercialType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\COMERCIAL;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/api", name="api_")
 */
class ComercialController extends FOSRestController
{
    /**
     * @Rest\Get("/comercials")
     *
     * @return Response
     */
    public function getComercialAction()
    {
        $repository = $this->getDoctrine()->getRepository(COMERCIAL::class);
        $comercial = $repository->findall();
        return $this->handleView($this->view($comercial));
    }
    /**
     * @Rest\Get("/comercial/{id}")
     *
     * @return Response
     */
    public function getComercialByIdAction($id)
    {
        $repository = $this->getDoctrine()->getRepository(COMERCIAL::class);
        $comercial =  $repository->find($id);
        if (!$comercial) {
            return $this->handleView($this->view(['Error' => 'Product not found'], Response::HTTP_CREATED));

            //throw $this->createNotFoundException(sprintf('No Movie found with id "%s"', $id ));
        }
        return $this->handleView($this->view($comercial));
    }


    /**
     * @Rest\Post("/comercial")
     *
     * @return Response
     */
    public function postComercialAction(Request $request)
    {
        $comercial = new COMERCIAL();
        $form = $this->createForm(ComercialType::class, $comercial);
        $data = json_decode($request->getContent(), true);
        $form->submit($data);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($comercial);
            $em->flush();
            return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
        }
        return $this->handleView($this->view($form->getErrors()));
    }
    /**
     * @Rest\Put("/comercial/{id}")
     *
     * @return Response
     */
    public function updateComercialAction(Request $request,$id)
    {
        $repository = $this->getDoctrine()->getRepository(COMERCIAL::class);
        $comercial =  $repository->find($id);

        if (!$comercial) {
            return $this->handleView($this->view(['Error' => 'Categoria not found'], Response::HTTP_CREATED));
            /*
            throw $this->createNotFoundException(sprintf(
                'No Movie found with id "%s"',
                $id
            ));*/
        }


        $_codigo__comercio = $request->get('_codigo__comercio');
        $_nombre__comercial = $request->get('_nombre__comercial');
        $_telefono__comercial = $request->get('_telefono__comercial');

        $em = $this->getDoctrine()->getManager();
        if(!empty($_codigo__comercio)){
            $comercial->setCODIGOCOMERCIO($_codigo__comercio);
        }
        if(!empty($_nombre__comercial)){
            $comercial->setNOMBRECOMERCIAL($_nombre__comercial);
        }
        if(!empty($_telefono__comercial)){
            $comercial->setTELEFONOCOMERCIAL($_telefono__comercial);
        }
         $em->flush();

        return $this->handleView($this->view(['Updated' => 'Successfully'], Response::HTTP_CREATED));

    }

    /**
     * @Rest\Delete("/comercial/{id}")
     *
     * @return Response
     */
    public function deleteComercialAction($id)
    {
        $repository = $this->getDoctrine()->getRepository(COMERCIAL::class);
        $comercial =  $repository->find($id);
        $em = $this->getDoctrine()->getManager();

        if (empty($comercial)) {
            return $this->handleView($this->view(['Error' => 'Categoria not found'], Response::HTTP_CREATED));
        }
        else {
            $em->remove($comercial);
            $em->flush();
        }
        return $this->handleView($this->view(['Deleted' => 'Successfully'], Response::HTTP_CREATED));

    }

}
