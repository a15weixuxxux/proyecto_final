<?php
namespace App\Controller;

use App\Entity\CLIENTE;
use App\Entity\COMERCIAL;
use App\Entity\EMPRESA;
use App\Entity\FACTURA;
use App\Entity\DEVOLUCION;
use function MongoDB\BSON\toJSON;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\VarDumper\Cloner\Data;


class DevolucioPdfGenerateController extends Controller
{
    /**
     * @Route("/devolucionPdf/{facturaId}/{type}", name="devolucionPdf")
     */
    public function pdfFacturaAction($facturaId,$type)
    {

        $repositoryF = $this->getDoctrine()->getRepository(FACTURA::class);
        $repositoryClient = $this->getDoctrine()->getRepository(CLIENTE::class);
        $repositoryComecial = $this->getDoctrine()->getRepository(COMERCIAL::class);
        $repositoryEmpresa = $this->getDoctrine()->getRepository(EMPRESA::class);
        $repositoryDevolucion = $this->getDoctrine()->getRepository(DEVOLUCION::class);

        $empresa = $repositoryEmpresa->findAll();
        $factura = $repositoryF->findBy(["facturaNumero"=>$facturaId]);
        $devolucion = $repositoryDevolucion->findBy(["numFactura"=>$facturaId]);
        if(!$factura){
            die("error, no encuentra la factura: ".$facturaId);
        }
        $clienteId = $factura[0]->getClienteId();
        $comercialId = $factura[0]->getComercialId();

        $productos =  json_decode($devolucion[0]->getProductos());

        $iva = number_format( $factura[0]->getIva(), 2, '.', '');
        $req =number_format( $factura[0]->getReq(), 2, '.', '');


        $cliente = $repositoryClient->findBy(["id"=>$clienteId]);
        $comercial = $repositoryComecial->findBy(["id"=>$comercialId]);



        $fechaPrint = date("Y-m-d H:i:s");

        $baseImponible = array_reduce($productos,function($carry, $productos)
        {
            $carry += $productos->{"subtotal"};
            return $carry;
        });


        $baseImponible =  number_format($baseImponible/(($iva/100)+1),2,'.','');
        $importeIva = number_format( $baseImponible * ($iva/100), 2, '.', '');
        $importeReq = number_format( $baseImponible * ($req/100), 2, '.', '');
        $subtotal =  number_format($baseImponible + $importeIva + $importeReq, 2, '.', '');

        $html = $this->renderView('factura/Devolucion.html.twig', [
            'cliente' => $cliente[0],'comercial' => $comercial[0],'productos' => $productos, 'factura'=> $factura[0],'iva'=>$iva,
            'req' =>$req,'fechaPrint' => $fechaPrint,'empresa'=>$empresa[0], 'baseImponible' =>$baseImponible,
            'importeIva'=>$importeIva, 'importeReq'=>$importeReq,'subtotal'=>$subtotal,'devolucion'=>$devolucion[0]
        ]);


        if($type == "view"){
            $snappy = $this->get('knp_snappy.pdf');
            $filename = "Devolucion".$factura[0]->getFacturaNumero();
            return new Response(
                $snappy->getOutputFromHtml($html),200,array(
                    'Content-Type'          => 'application/pdf',
                    'Content-Disposition'   => 'inline; filename="'.$filename.'.pdf"'
                )
            );
        }else if($type == "download"){
             return new PdfResponse(
                 $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
                 "Devolucion".$factura[0]->getFacturaNumero().'.pdf'
             );
        }else{
            return new Response('Page not found.', Response::HTTP_NOT_FOUND);
        }
    }


}