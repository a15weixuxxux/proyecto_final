<?php
/**
 * Created by PhpStorm.
 * User: ausias
 * Date: 10/04/19
 * Time: 11:39
 */

namespace App\Controller;

use App\Form\EmpresaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\EMPRESA;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/api", name="api_")
 */
class EmpresaController extends FOSRestController
{
    /**
     * @Rest\Get("/empresas")
     *
     * @return Response
     */
    public function getAction()
    {
        $repository = $this->getDoctrine()->getRepository(EMPRESA::class);
        $empresa = $repository->findall();
        return $this->handleView($this->view($empresa));
    }
    /**
     * @Rest\Get("/empresa/{id}")
     *
     * @return Response
     */
    public function getByIdAction($id)
    {
        $repository = $this->getDoctrine()->getRepository(EMPRESA::class);
        $empresa =  $repository->find($id);
        if (!$empresa) {
            return $this->handleView($this->view(['Error' => 'Product not found'], Response::HTTP_CREATED));

            //throw $this->createNotFoundException(sprintf('No Movie found with id "%s"', $id ));
        }
        return $this->handleView($this->view($empresa));
    }


    /**
     * @Rest\Post("/empresa")
     *
     * @return Response
     */
    public function postAction(Request $request)
    {
        $empresa = new EMPRESA();
        $form = $this->createForm(EmpresaType::class, $empresa);
        $data = json_decode($request->getContent(), true);
        $form->submit($data);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($empresa);
            $em->flush();
            return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
        }
        return $this->handleView($this->view($form->getErrors()));
    }
    /**
     * @Rest\Put("/empresa/{id}")
     *
     * @return Response
     */
    public function updateAction(Request $request,$id)
    {
        $repository = $this->getDoctrine()->getRepository(EMPRESA::class);
        $empresa =  $repository->find($id);

        if (!$empresa) {
            return $this->handleView($this->view(['Error' => 'Categoria not found'], Response::HTTP_CREATED));
            /*
            throw $this->createNotFoundException(sprintf(
                'No Movie found with id "%s"',
                $id
            ));*/
        }


        $nom = $request->get('nom');
        $cp = $request->get('cp');
        $provincia = $request->get('provincia');
        $ciudad = $request->get('ciudad');
        $direccion = $request->get('direccion');
        $telefono = $request->get('telefono');
        $mobil = $request->get('mobil');
        $cif = $request->get('cif');


        $em = $this->getDoctrine()->getManager();
        if(!empty($nom)){
            $empresa->setNom($nom);
        }
        if(!empty($cp)){
            $empresa->setCp($cp);
        }
        if(!empty($provincia)){
            $empresa->setProvincia($provincia);
        }
        if(!empty($ciudad)){
            $empresa->setCiudad($ciudad);
        }
        if(!empty($direccion)){
            $empresa->setDireccion($direccion);
        }
        if(!empty($telefono)){
            $empresa->setTelefono($telefono);
        }
        if(!empty($mobil)){
            $empresa->setMobil($mobil);
        }
        if(!empty($cif)){
            $empresa->setCif($cif);
        }
         $em->flush();

        return $this->handleView($this->view(['Updated' => 'Successfully'], Response::HTTP_CREATED));

    }

    /**
     * @Rest\Delete("/empresa/{id}")
     *
     * @return Response
     */
    public function deleteComercialAction($id)
    {
        $repository = $this->getDoctrine()->getRepository(EMPRESA::class);
        $empresa =  $repository->find($id);
        $em = $this->getDoctrine()->getManager();

        if (empty($empresa)) {
            return $this->handleView($this->view(['Error' => 'Categoria not found'], Response::HTTP_CREATED));
        }
        else {
            $em->remove($empresa);
            $em->flush();
        }
        return $this->handleView($this->view(['Deleted' => 'Successfully'], Response::HTTP_CREATED));

    }

}
