<?php
/**
 * Created by PhpStorm.
 * User: ausias
 * Date: 10/04/19
 * Time: 11:39
 */

namespace App\Controller;

use App\Form\AlbaransType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\ALBARAN;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/api", name="api_")
 */
class AlbaranController extends FOSRestController
{
    /**
     * @Rest\Get("/albarans")
     *
     * @return Response
     */
    public function getAction()
    {
        $repository = $this->getDoctrine()->getRepository(ALBARAN::class);
        $albaran = $repository->findall();
        return $this->handleView($this->view($albaran));
    }
    /**
     * @Rest\Get("/albaran/{id}")
     *
     * @return Response
     */
    public function getByIdAction($id)
    {
        $repository = $this->getDoctrine()->getRepository(ALBARAN::class);
        $albaran =  $repository->find($id);
        if (!$albaran) {
            return $this->handleView($this->view(['Error' => 'factura not found'], Response::HTTP_CREATED));

            //throw $this->createNotFoundException(sprintf('No Movie found with id "%s"', $id ));
        }
        return $this->handleView($this->view($albaran));
    }


    /**
     * @Rest\Post("/albaran")
     *
     * @return Response
     */
    public function postAction(Request $request)
    {
        $albaran = new ALBARAN();
        $form = $this->createForm(AlbaransType::class, $albaran);
        $data = json_decode($request->getContent(), true);
        $form->submit($data);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($albaran);
            $em->flush();
            return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
        }
        return $this->handleView($this->view($form->getErrors()));
    }
    /**
     * @Rest\Put("/albaran/{id}")
     *
     * @return Response
     */
    public function updateAction(Request $request,$id)
    {
        $repository = $this->getDoctrine()->getRepository(ALBARAN::class);
        $albaran =  $repository->find($id);

        if (!$albaran) {
            return $this->handleView($this->view(['Error' => 'Categoria not found'], Response::HTTP_CREATED));
            /*
            throw $this->createNotFoundException(sprintf(
                'No Movie found with id "%s"',
                $id
            ));*/
        }


        $albalan_numero = $request->get('albalan_numero');
        $fecha = $request->get('fecha');
        $productos = $request->get('productos');
        $cliente_id = $request->get('cliente_id');



        $em = $this->getDoctrine()->getManager();
        if(!empty($albalan_numero)){
            $albaran->setAlbalanNumero($albalan_numero);
        }
        if(!empty($fecha)){
            $albaran->setFecha($fecha);
        }
        if(!empty($productos)){
            $albaran->setProductos($productos);
        }
        if(!empty($cliente_id)){
            $albaran->setClienteId($cliente_id);
        }
        $em->flush();

        return $this->handleView($this->view(['Updated' => 'Successfully'], Response::HTTP_CREATED));

    }

    /**
     * @Rest\Delete("/albaran/{id}")
     *
     * @return Response
     */
    public function deleteComercialAction($id)
    {
        $repository = $this->getDoctrine()->getRepository(ALBARAN::class);
        $factura =  $repository->find($id);
        $em = $this->getDoctrine()->getManager();

        if (empty($factura)) {
            return $this->handleView($this->view(['Error' => 'Categoria not found'], Response::HTTP_CREATED));
        }
        else {
            $em->remove($factura);
            $em->flush();
        }
        return $this->handleView($this->view(['Deleted' => 'Successfully'], Response::HTTP_CREATED));

    }

}
