<?php
/**
 * Created by PhpStorm.
 * User: ausias
 * Date: 10/04/19
 * Time: 11:39
 */

namespace App\Controller;
use App\Form\ProductoType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Entity\PRODUCTOS;

/**
 * Producto controller.
 * @Route("/api", name="api_")
 */
class ProductoController extends FOSRestController
{
    /**
     * Lists all Productos.
     * @Rest\Get("/productos")
     *
     * @return Response
     */
    public function getProductosAction()
    {
        $repository = $this->getDoctrine()->getRepository(PRODUCTOS::class);
        $productos = $repository->findall();
        return $this->handleView($this->view($productos));
    }

    /**
     * @Rest\Get("/producto/{id}")
     *
     * @return Response
     */
    public function getProductosByIdAction($id)
    {
        $repository = $this->getDoctrine()->getRepository(PRODUCTOS::class);
        $producto =  $repository->find($id);
        if (!$producto) {
            return $this->handleView($this->view(['Error' => 'Product not found'], Response::HTTP_CREATED));

            //throw $this->createNotFoundException(sprintf('No Movie found with id "%s"', $id ));
        }
        return $this->handleView($this->view($producto));
    }
    /**
     * @Rest\Post("/producto")
     *
     * @return Response
     */
    public function postProductoAction(Request $request)
    {
        $producto = new PRODUCTOS();
        $form = $this->createForm(ProductoType::class, $producto);
        $data = json_decode($request->getContent(), true);
        $form->submit($data);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($producto);
            $em->flush();
            return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
        }
        return $this->handleView($this->view($form->getErrors()));
    }

    /**
     * @Rest\Put("/producto/{id}")
     *
     * @return Response
     */
    public function updateProductoAction(Request $request,$id)
    {
        $repository = $this->getDoctrine()->getRepository(PRODUCTOS::class);
        $producto =  $repository->find($id);

        if (!$producto) {
            return $this->handleView($this->view(['Error' => 'Product not found'], Response::HTTP_CREATED));

            //throw $this->createNotFoundException(sprintf('No Movie found with id "%s"', $id ));
        }

        $_codigo__producto = $request->get('_codigo__producto');
        $_nombre__producto = $request->get('_nombre__producto');
        $_precio__compra = $request->get('_precio__compra');
        $_pvp = $request->get('_pvp');
        $_cantidad__pack = $request->get('_cantidad__pack');
        $_descuento__producto = $request->get('_descuento__producto');
        $_stock = $request->get('_stock');
        $_aviso__stock = $request->get('_aviso__stock');
        $_categoria = $request->get('_categoria');
        $_codigo__barra = $request->get('_codigo__barra');


        $em = $this->getDoctrine()->getManager();
        if(!empty($_codigo__producto) || $_codigo__producto == 0){
            $producto->setCODIGOPRODUCTO($_codigo__producto);
        }
        if(!empty($_nombre__producto) || $_nombre__producto == 0){
            $producto->setNOMBREPRODUCTO($_nombre__producto);
        }
        if(!empty($_precio__compra) || $_precio__compra == 0){
            $producto->setPRECIOCOMPRA($_precio__compra);
        }
        if(!empty($_pvp) || $_pvp == 0){
            $producto->setPVP($_pvp);
        }
        if(!empty($_cantidad__pack) || $_cantidad__pack == 0){
            $producto->setCANTIDADPACK($_cantidad__pack);
        }
        if(!empty($_descuento__producto) || $_descuento__producto == 0){
            $producto->setDESCUENTOPRODUCTO($_descuento__producto);
        }
        if(!empty($_stock) || $_stock == 0){
            $producto->setSTOCK($_stock);
        }
        if(!empty($_aviso__stock) || $_aviso__stock == 0){
            $producto->setAVISOSTOCK($_aviso__stock);
        }
        if(!empty($_categoria) || $_categoria == 0){
            $producto->setCATEGORIA($_categoria);
        }
        if(!empty($_codigo__barra) || $_codigo__barra == 0){
            $producto->setCODIGOBARRA($_codigo__barra);
        }

        $em->persist($producto);
        $em->flush();
        return $this->handleView($this->view(['Updated' => 'Successfully'], Response::HTTP_CREATED));

    }

    /**
     * @Rest\Put("/addProductoStock/{id}")
     *
     * @return Response
     */
    public function AddProductoStock(Request $request,$id)
    {
        $repository = $this->getDoctrine()->getRepository(PRODUCTOS::class);
        $producto =  $repository->findBy(["CODIGO_BARRA"=>$id]);

        $producto = $producto[0];
        if (!$producto) {
            return $this->handleView($this->view(['Error' => 'Product not found'], Response::HTTP_CREATED));

            //throw $this->createNotFoundException(sprintf('No Movie found with id "%s"', $id ));
        }

        $_stock = $request->get('_stock');

        $em = $this->getDoctrine()->getManager();

        if(!empty($_stock) || $_stock == 0){
            $_stock += $producto->getSTOCK();
            $producto->setSTOCK($_stock);
        }


        $em->persist($producto);
        $em->flush();
        return $this->handleView($this->view(['Updated' => 'Successfully'], Response::HTTP_CREATED));

    }

    /**
     * @Rest\Delete("/producto/{id}")
     *
     * @return Response
     */
    public function deleteProductoAction($id)
    {
        $repository = $this->getDoctrine()->getRepository(PRODUCTOS::class);
        $producto =  $repository->find($id);
        $em = $this->getDoctrine()->getManager();

        if (empty($producto)) {
            return $this->handleView($this->view(['Error' => 'Categoria not found'], Response::HTTP_CREATED));
        }
        else {
            $em->remove($producto);
            $em->flush();
        }
        return $this->handleView($this->view(['Deleted' => 'Successfully'], Response::HTTP_CREATED));

    }


}
