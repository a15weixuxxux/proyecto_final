<?php
/**
 * Created by PhpStorm.
 * User: ausias
 * Date: 10/04/19
 * Time: 11:39
 */

namespace App\Controller;
use App\Form\CategoriaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\CATEGORIA;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Categoria controller.
 * @Route("/api", name="api_")
 */
class CategoriaController extends FOSRestController
{
    /**
     * Lists all CATEGORIA.
     * @Rest\Get("/categorias")
     *
     * @return Response
     */
    public function getCategoriaAction()
    {
        $repository = $this->getDoctrine()->getRepository(CATEGORIA::class);
        $categorias = $repository->findall();
        return $this->handleView($this->view($categorias));
    }
    /**
     * @Rest\Get("/categoria/{id}")
     *
     * @return Response
     */
    public function getCategoriaByIdAction($id)
    {
        $repository = $this->getDoctrine()->getRepository(CATEGORIA::class);
        $producto =  $repository->find($id);
        if (!$producto) {
            return $this->handleView($this->view(['Error' => 'Product not found'], Response::HTTP_CREATED));

            //throw $this->createNotFoundException(sprintf('No Movie found with id "%s"', $id ));
        }
        return $this->handleView($this->view($producto));
    }


    /**
     * Create CATEGORIA.
     * @Rest\Post("/categoria")
     *
     * @return Response
     */
    public function postCategoriaAction(Request $request)
    {
        $categorias = new CATEGORIA();
        $form = $this->createForm(CategoriaType::class, $categorias);
        $data = json_decode($request->getContent(), true);
        $form->submit($data);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($categorias);
            $em->flush();
            return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
        }
        return $this->handleView($this->view($form->getErrors()));
    }
    /**
     * @Rest\Put("/categoria/{id}")
     *
     * @return Response
     */
    public function updateCategoriaAction(Request $request,$id)
    {
        $repository = $this->getDoctrine()->getRepository(CATEGORIA::class);
        $categorias =  $repository->find($id);

        if (!$categorias) {
            return $this->handleView($this->view(['Error' => 'Categoria not found'], Response::HTTP_CREATED));
            /*
            throw $this->createNotFoundException(sprintf(
                'No Movie found with id "%s"',
                $id
            ));*/
        }

        $name = $request->get('name');

        $em = $this->getDoctrine()->getManager();
        if(!empty($name)){
            $categorias->setName($name);
            $em->flush();
        }

        return $this->handleView($this->view(['Updated' => 'Successfully'], Response::HTTP_CREATED));

    }

    /**
     * @Rest\Delete("/categoria/{id}")
     *
     * @return Response
     */
    public function deleteCategoriaAction($id)
    {
        $repository = $this->getDoctrine()->getRepository(CATEGORIA::class);
        $categorias =  $repository->find($id);
        $em = $this->getDoctrine()->getManager();

        if (empty($categorias)) {
            return $this->handleView($this->view(['Error' => 'Categoria not found'], Response::HTTP_CREATED));
        }
        else {
            $em->remove($categorias);
            $em->flush();
        }
        return $this->handleView($this->view(['Deleted' => 'Successfully'], Response::HTTP_CREATED));

    }

    //////////////////////////////////////////
    /**
     * @Route("/addCategoria")
     */
    public function addCategoria(Request $request)
    {
        $categoria = new Categoria();

        $form = $this->createFormBuilder($categoria)
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, ['label' => 'Create Task'])
            ->getForm();


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $categoria = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($categoria);
            $entityManager->flush();
            return new Response(
                '<html><body>ok</body></html>'
            );
        }

        return $this->render('categoria/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    }
