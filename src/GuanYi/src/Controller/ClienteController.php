<?php
/**
 * Created by PhpStorm.
 * User: ausias
 * Date: 10/04/19
 * Time: 11:39
 */

namespace App\Controller;
use App\Form\ClienteType;
use App\Form\ProductoType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Entity\CLIENTE;

/**
 * Cliente controller.
 * @Route("/api", name="api_")
 */
class ClienteController extends FOSRestController
{
    /**
     * @Rest\Get("/clientes")
     *
     * @return Response
     */
    public function getClienteAction()
    {
        $repository = $this->getDoctrine()->getRepository(CLIENTE::class);
        $cliente = $repository->findall();
        return $this->handleView($this->view($cliente));
    }

    /**
     * @Rest\Get("/cliente/{id}")
     *
     * @return Response
     */
    public function getClienteByIdAction($id)
    {
        $repository = $this->getDoctrine()->getRepository(CLIENTE::class);
        $cliente =  $repository->find($id);
        if (!$cliente) {
            return $this->handleView($this->view(['Error' => 'Product not found'], Response::HTTP_CREATED));

            //throw $this->createNotFoundException(sprintf('No Movie found with id "%s"', $id ));
        }
        return $this->handleView($this->view($cliente));
    }
    /**
     * @Rest\Post("/cliente")
     *
     * @return Response
     */
    public function postClienteAction(Request $request)
    {
        $cliente = new CLIENTE();
        $form = $this->createForm(ClienteType::class, $cliente);
        $data = json_decode($request->getContent(), true);
        $form->submit($data);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($cliente);
            $em->flush();
            return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
        }
        return $this->handleView($this->view($form->getErrors()));
    }

    /**
     * @Rest\Put("/cliente/{id}")
     *
     * @return Response
     */
    public function updateProductoAction(Request $request,$id)
    {
        $repository = $this->getDoctrine()->getRepository(CLIENTE::class);
        $cliente =  $repository->find($id);

        if (!$cliente) {
            return $this->handleView($this->view(['Error' => 'Product not found'], Response::HTTP_CREATED));

            //throw $this->createNotFoundException(sprintf('No Movie found with id "%s"', $id ));
        }
        $_codigo__cliente = $request->get('_codigo__cliente');
        $_nombre__cliente = $request->get('_nombre__cliente');
        $_nombre__contacto = $request->get('_nombre__contacto');
        $_telefono__contacto = $request->get('_telefono__contacto');
        $_cif__dni__nie = $request->get('_cif__dni__nie');
        $_direccion = $request->get('_direccion');
        $_cp = $request->get('_cp');
        $_ciudad = $request->get('_ciudad');
        $_provincia = $request->get('_provincia');
        $_pais = $request->get('_pais');
        $_telefono1 = $request->get('_telefono1');
        $_telefono2 = $request->get('_telefono2');
        $_movil = $request->get('_movil');
        $_fax = $request->get('_fax');
        $_email = $request->get('_email');
        $_direccion__envio = $request->get('_direccion__envio');
        $_cp__envio = $request->get('_cp__envio');
        $_ciudad__envio = $request->get('_ciudad__envio');
        $_provincia__envio = $request->get('_provincia__envio');
        $_pais__envio = $request->get('_pais__envio');
        $_descuento__cliente = $request->get('_descuento__cliente');



        $em = $this->getDoctrine()->getManager();
        if(!empty($_codigo__cliente)){
            $cliente->setCODIGOCLIENTE($_codigo__cliente);
        }
        if(!empty($_nombre__cliente)){
            $cliente->setNOMBRECLIENTE($_nombre__cliente);
        }
        if(!empty($_nombre__contacto)){
            $cliente->setNOMBRECONTACTO($_nombre__contacto);
        }
        if(!empty($_telefono__contacto)){
            $cliente->setTELEFONOCONTACTO($_telefono__contacto);
        }
        if(!empty($_cif__dni__nie)){
            $cliente->setCIFDNINIE($_cif__dni__nie);
        }
        if(!empty($_direccion)){
            $cliente->setDIRECCION($_direccion);
        }
        if(!empty($_cp)){
            $cliente->setCP($_cp);
        }
        if(!empty($_ciudad)){
            $cliente->setCIUDAD($_ciudad);
        }
        if(!empty($_provincia)){
            $cliente->setPROVINCIA($_provincia);
        }
        if(!empty($_pais)){
            $cliente->setPAIS($_pais);
        }
        if(!empty($_telefono1)){
            $cliente->setTELEFONO1($_telefono1);
        }
        if(!empty($_telefono2) ){
            $cliente->setTELEFONO2($_telefono2);
        }
        if(!empty($_movil)){
            $cliente->setMOVIL($_movil);
        }
        if(!empty($_fax) ){
            $cliente->setFAX($_fax);
        }
        if(!empty($_email)){
            $cliente->setEMAIL($_email);
        }
        if(!empty($_direccion__envio) ){
            $cliente->setDIRECCIONENVIO($_direccion__envio);
        }
        if(!empty($_cp__envio) ){
            $cliente->setCPENVIO($_cp__envio);
        }
        if(!empty($_ciudad__envio)){
            $cliente->setCIUDADENVIO($_ciudad__envio);
        }
        if(!empty($_provincia__envio) ){
            $cliente->setPROVINCIAENVIO($_provincia__envio);
        }
        if(!empty($_pais__envio) ){
            $cliente->setPAISENVIO($_pais__envio);
        }
        if(!empty($_descuento__cliente) ){
            $cliente->setDESCUENTOCLIENTE($_descuento__cliente);
        }

        $em->persist($cliente);
        $em->flush();
        return $this->handleView($this->view(['Updated' => 'Successfully'], Response::HTTP_CREATED));

    }

    /**
     * @Rest\Delete("/cliente/{id}")
     *
     * @return Response
     */
    public function deleteClienteAction($id)
    {
        $repository = $this->getDoctrine()->getRepository(CLIENTE::class);
        $cliente =  $repository->find($id);
        $em = $this->getDoctrine()->getManager();

        if (empty($cliente)) {
            return $this->handleView($this->view(['Error' => 'Categoria not found'], Response::HTTP_CREATED));
        }
        else {
            $em->remove($cliente);
            $em->flush();
        }
        return $this->handleView($this->view(['Deleted' => 'Successfully'], Response::HTTP_CREATED));

    }


}
